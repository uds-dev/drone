data "template_file" "drone_server_task_definition" {
  template = file("${path.module}/task-definitions/drone-server.json.tpl")

  vars = {
    db_name                       = var.db_name
    db_username                   = var.db_username
    db_password                   = random_string.db_password.result
    db_host                       = aws_db_instance.drone.endpoint
    container_cpu                 = var.drone_server_container_cpu
    container_memory              = var.drone_server_container_memory
    log_group_region              = var.aws_region
    log_group_drone_server        = aws_cloudwatch_log_group.drone_server.name
    drone_host                    = var.drone_host
    drone_itbucket_client_id      = var.drone_bitbucket_client_id
    drone_bitbucket_client_secret = var.drone_bitbucket_client_secret
    drone_server_port             = var.drone_server_port
    drone_server_proto            = var.drone_server_proto
    drone_rpc_secret              = var.drone_rpc_secret
    drone_docker_repo             = var.drone_docker_repo
    drone_server_version          = var.drone_server_version
    drone_user_filter             = local.drone_user_filter
    drone_admin_username          = var.drone_admin_username
    drone_log_file_max_size       = var.drone_log_file_max_size
    drone_s3_bucket               = aws_s3_bucket.logs_bucket.id
  }
}

data "aws_caller_identity" "current" {}

resource "aws_ecs_task_definition" "drone_server" {
  family                   = "drone-server"
  container_definitions    = data.template_file.drone_server_task_definition.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"

  task_role_arn      = aws_iam_role.ecs_task.arn
  # Uses the default IAM Role "ecsTaskExecutionRole" to pull private images
  execution_role_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/ecsTaskExecutionRole"

  cpu    = var.drone_server_task_cpu
  memory = var.drone_server_task_memory
}

resource "aws_ecs_service" "drone_server" {
  name            = "drone-server"
  cluster         = var.aws_ecs_cluster_id
  task_definition = aws_ecs_task_definition.drone_server.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = ["${aws_security_group.drone_server.id}"]
    subnets          = var.vpc_subnets_public_ids
    assign_public_ip = true
  }
  load_balancer {
    target_group_arn = aws_alb_target_group.drone.id
    container_name   = "drone-server"
    container_port   = "80"
  }
  depends_on = [
    aws_alb_listener.front_end_80,
    aws_alb_listener.front_end_443,
  ]
}
