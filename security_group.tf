resource "aws_security_group" "web" {
  description = "controls access to the application ELB"

  vpc_id = var.vpc_id
  name   = "drone-lb"

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "drone_server" {
  description = "controls direct access to application instances"
  vpc_id      = var.vpc_id
  name        = "drone-server-task-sg"

  ingress {
    protocol  = "tcp"
    from_port = var.drone_server_port
    to_port   = var.drone_server_port

    security_groups = [
      "${aws_security_group.web.id}",
    ]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "db" {
  name        = "drone-db-sg"
  description = "Allow all inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port = 5432
    to_port   = 5432
    protocol  = "TCP"

    security_groups = [
      "${aws_security_group.drone_server.id}",
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
