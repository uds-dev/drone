data "template_file" "drone_agent_task_definition_windows" {
  template = file("${path.module}/task-definitions/drone-agent-windows.json.tpl")

  vars = {
    log_group_region           = var.aws_region
    log_group_drone_agent      = aws_cloudwatch_log_group.drone_agent.name
    drone_server               = var.drone_host
    drone_rpc_secret           = var.drone_rpc_secret
    drone_rpc_proto            = var.drone_rpc_proto
    drone_runner_label_windows = var.drone_runner_label_windows
    drone_agent_version        = var.drone_agent_version
    container_cpu              = var.drone_agent_container_cpu
    container_memory           = var.drone_agent_container_memory
  }
}

resource "aws_ecs_task_definition" "drone_agent_windows" {
  family                = "drone-agent-windows"
  container_definitions = data.template_file.drone_agent_task_definition_windows.rendered

  volume {
    name      = "dockersock"
    host_path = "\\\\.\\pipe\\docker_engine"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.os-type=='windows'"
  }

}

resource "aws_ecs_service" "drone_agent_windows" {
  name            = "drone-agent-windows"
  cluster         = var.aws_ecs_cluster_id
  desired_count   = var.drone_desired_count_agent_windows
  task_definition = aws_ecs_task_definition.drone_agent_windows.arn

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.os-type=='windows'"
  }

}
