data "template_file" "drone_agent_task_definition" {
  template = file("${path.module}/task-definitions/drone-agent.json.tpl")

  vars = {
    log_group_region      = var.aws_region
    log_group_drone_agent = aws_cloudwatch_log_group.drone_agent.name
    drone_server          = var.drone_host
    drone_rpc_secret      = var.drone_rpc_secret
    drone_rpc_proto       = var.drone_rpc_proto
    drone_runner_label    = var.drone_runner_label
    drone_agent_version   = var.drone_agent_version
    container_cpu         = var.drone_agent_container_cpu
    container_memory      = var.drone_agent_container_memory
  }
}

resource "aws_ecs_task_definition" "drone_agent" {
  family                = "drone-agent"
  container_definitions = data.template_file.drone_agent_task_definition.rendered

  volume {
    name      = "dockersock"
    host_path = "/var/run/docker.sock"
  }

  volume {
    name      = "dockerhub"
    host_path = "/root/.docker/config.json"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.os-type=='linux'"
  }
}

resource "aws_ecs_service" "drone_agent" {
  name            = "drone-agent"
  cluster         = var.aws_ecs_cluster_id
  desired_count   = var.drone_desired_count_agent
  task_definition = aws_ecs_task_definition.drone_agent.arn

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.os-type=='linux'"
  }

  ordered_placement_strategy {
    type = "spread"
    field = "instanceId"
  }
}
