[
  {
    "name": "drone-agent",
    "image": "drone/drone-runner-docker:${drone_agent_version}",
    "cpu": ${container_cpu},
    "memory": ${container_memory},
    "environment": [
      { "name": "DRONE_RPC_HOST", "value": "${drone_server}" },
      { "name": "DRONE_RPC_PROTO", "value": "${drone_rpc_proto}" },
      { "name": "DRONE_RPC_SECRET", "value": "${drone_rpc_secret}" },
      { "name": "DRONE_RUNNER_LABELS", "value": "${drone_runner_label}" }
    ],
    "mountPoints": [
      { "sourceVolume": "dockersock", "containerPath": "/var/run/docker.sock" },
      { "sourceVolume": "dockerhub", "containerPath": "/root/.docker/config.json" }
    ],

    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-stream-prefix": "drone",
        "awslogs-group": "${log_group_drone_agent}",
        "awslogs-region": "${log_group_region}"
      }
    }
  }
]
