[
  {
    "name": "drone-server",
    "image": "${drone_docker_repo}:${drone_server_version}",
    "cpu": ${container_cpu},
    "memory": ${container_memory},
    "portMappings": [{
      "containerPort": ${drone_server_port},
      "protocol": "tcp"
    }],
    "environment": [
      { "name": "DRONE_SERVER_HOST", "value": "${drone_host}" },
      { "name": "DRONE_BITBUCKET_CLIENT_ID", "value": "${drone_itbucket_client_id}" },
      { "name": "DRONE_BITBUCKET_CLIENT_SECRET", "value": "${drone_bitbucket_client_secret}" },
      { "name": "DRONE_RPC_SECRET", "value": "${drone_rpc_secret}" },
      { "name": "DRONE_SERVER_PROTO", "value": "${drone_server_proto}" },
      { "name": "DRONE_SERVER_PORT", "value": ":${drone_server_port}" },
      { "name": "DRONE_DATABASE_DRIVER", "value": "postgres" },
      { "name": "DRONE_DATABASE_DATASOURCE", "value": "postgres://${db_username}:${db_password}@${db_host}/${db_name}?sslmode=disable"},
      { "name": "DRONE_USER_FILTER", "value": "${drone_user_filter}" },
      { "name": "DRONE_USER_CREATE", "value": "username:${drone_admin_username},admin:true" },
      { "name": "DRONE_LOG_FILE_MAX_SIZE", "value": "${drone_log_file_max_size}" },
      { "name": "DRONE_S3_BUCKET", "value": "${drone_s3_bucket}" }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-stream-prefix": "drone",
        "awslogs-group": "${log_group_drone_server}",
        "awslogs-region": "${log_group_region}"
      }
    }
  }
]
