### ---------------------------------------------------
### S3 Buckets
### ---------------------------------------------------
resource "aws_s3_bucket" "logs_bucket" {
  bucket        = "drone-logs-binah"
  acl           = "private"
  force_destroy = true
  versioning {
    enabled = false
  }
}
### ---------------------------------------------------
