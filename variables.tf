variable "vpc_subnets_private_ids" {
  type = list
}

variable "vpc_subnets_public_ids" {
  type = list
}

variable "vpc_id" {
  type = string
}

variable "aws_ecs_cluster_id" {
  type = string
}
variable "aws_region" {
  description = "aws region."
  default     = "eu-west-1"
}
variable "identifier" {
  default     = "drone-rds"
  description = "Identifier for your DB"
}

variable "storage" {
  default     = "100"
  description = "Storage size in GB"
}

variable "engine" {
  default     = "postgres"
  description = "Engine type, example values mysql, postgres"
}

variable "engine_version" {
  description = "Engine version"
  default = {
    postgres = "11.12"
  }
}
variable "instance_class" {
  default     = "db.t2.medium"
  description = "Instance class"
}

variable "db_name" {
  default     = "drone"
  description = "db name"
}

variable "db_username" {
  default     = "drone"
  description = "database user name"
}

variable "alb_port" {
  description = "ALB frond end port"
  default     = "80"
}

variable "drone_server_task_cpu" {
  description = "cpu usage of ecs task"
  default     = 512
}

variable "drone_agent_task_cpu" {
  description = "cpu usage of ecs task"
  default     = 512
}

variable "drone_server_task_memory" {
  description = "memory usage of ecs task"
  default     = 2048
}

variable "drone_agent_task_memory" {
  description = "memory usage of ecs task"
  default     = 2048
}

variable "drone_server_container_cpu" {
  description = "cpu usage of ecs container"
  default     = 512
}

variable "drone_agent_container_cpu" {
  description = "cpu usage of ecs container"
  default     = 512
}

variable "drone_server_container_memory" {
  description = "memory usage of ecs container"
  default     = 1024
}

variable "drone_agent_container_memory" {
  description = "memory usage of ecs container"
  default     = 1024
}

variable "drone_server_port" {
  description = "drone server port."
  default     = "80"
}

variable "drone_bitbucket_client_id" {
  description = "drone bitbucket client."
}

variable "drone_bitbucket_client_secret" {
  description = "drone bitbucket secret."
}

variable "drone_organization" {
  description = "used in DRONE_USER_FILTER"
}

variable "drone_server_proto" {
  description = "drone server proto."
  default     = "https"
}

variable "drone_host" {
  description = "drone server host"
  default     = "drone.binahai.com"
}

variable "drone_admin_username" {
  description = "main admin username for drone"
  type        = string
}

variable "drone_rpc_proto" {
  description = "drone rpc proto."
  default     = "http"
}

variable "drone_runner_label" {
  description = "drone docker runner label."
  default     = "runner:docker"
}

variable "drone_runner_label_windows" {
  description = "windows drone docker runner label."
  default     = "runner:docker-windows"
}

variable "drone_rpc_secret" {
  description = "drone rpc secret."
}

variable "drone_agent_version" {
  description = "drone agent version."
}

variable "drone_docker_repo" {
  description = "Our private AWS ECR repo, e.g. 'drone/drone'"
}

variable "drone_server_version" {
  description = "drone server version."
}

variable "drone_desired_count_agent" {
  description = "drone agent desired."
}

variable "drone_desired_count_agent_windows" {
  description = "windows drone agent desired."
}

variable "drone_log_file_max_size" {
  description = "size of drone logs"
}

locals {
  drone_user_filter = var.drone_organization
}
